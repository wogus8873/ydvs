//dom2.js

let pTag = document.createElement("p");//<p> Hello
let txt = document.createTextNode("Hello");
pTag.appendChild(txt);

// window.prompt("경고창")
// console.log(window)
// window.onclick = function() {
//     console.log("윈도우클릭")
// }

window.onload = function(){  //스크립트 파일위치를 헤드에 올려놔서 읽어올 수 없었는데(순서대로 읽어서) 이걸로 해결
    document.querySelector(".show").appendChild(pTag);
                                               
    //버튼선택 > 클릭 이벤트 등록.
    let btn = document.querySelector("#btn")
    btn.onclick = function() {
       //이름값 가져오는 부분 id
       let name = document.querySelector("#name").value;
       let age = document.querySelector("input[name=age]").value;
       if(!name || !age){
        window.alert("값을 입력하세요");
        return;
       }

       //li 생성 . text: name, age -> 자식등록
       let liTag = document.createElement('li');
       let txt = document.createTextNode(name + ","+age);
       liTag.append(txt);
       document.getElementById('list').append(liTag);

       //초기화 
       document.querySelector("#name").value = "";
       document.querySelector("#age").value = "";


    }
    console.log(btn)
}