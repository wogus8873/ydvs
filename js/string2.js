// //
// //trim(), trimStart() , trimEnd()  양쪽 왼쪽 오른쪽 공백 제거
// let str = "  Hello, World   ";
// console.log(str);
// let result = str.trim();//tirm 양 끝단 공백제거
// console.log(result);

// //대문자, 소문자 : toUpperCase, toLowerCase 
// result = str.toUpperCase()//반환값이 문자열
//             //.toLowerCase()
//             .trim();
// result = str.trim().substring(0,5).toUpperCase() + str.trim().substring(5)
// console.log(result);

// // result = str.substring(0,7) + str.substring(7).toLowerCase()
// //문자열을 대체 (replace)
// //"  Hellow, World    "
// result = str.replace("World","world");
// console.log(result);

// str = ["김기훈" , "김근연" , "이소라", "오수현", "박김희"]
// function findKim(kim){
//     for(let name of str){
//         if(name.substring(0,1)==kim){  //string.endsWith마지막글자가어쩌구
//             console.log(name)
//         }
//     }
// }


// result = findKim("김")

// //substring 과 비슷함 slice (시작인덱스, 종료인덱스) -값도 넣을 수 있음

// str = "Hello, World"; //문자열12 
// result = str.slice(-5,-1);  //-값이 들어가면 12- 5 , 12-1 = 7 11 이 됨
// console.log(result);

//주민번호(950131-1234567)
// function checkGender(jumin){ //1번
//     let gend 
//         if(jumin.substring(7,8)%2 == 1){
//             gend="남자"
//         }else if(jumin.substring(7,8)%2 == 0) {
//             gend="여자"
//         }

//     return gend
// }

function checkGender(jumin){ //2번
    if(jumin.substring(7,8) == 1 || jumin.substring(7,8) == 3){
        return "남자"
    }else if(jumin.substring(7,8) == 2 || jumin.substring(7,8) == 4){
        return "여자"
    }
}

result = checkGender("950131-1234567");
console.log(result)
result = checkGender("950131-2234567");
console.log(result)
result = checkGender("950131-4234567");
console.log(result)
result = checkGender("950131-3234567");
console.log(result)