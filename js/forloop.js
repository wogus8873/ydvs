// forloop.js
//초기값, 조건식, 증가 혹은 감소치
for (let i = 10; i >= 0; i-= 2) {
    console.log(i);
    //i += 2 이렇게 증가치는 for 안에 있기만 하면 됨.
}
console.log("end of for");
for (let i = 0; i < 10;) {
    i += 1
    console.log(i);
}
console.log("end of for1");
for (let i = 1; i <= 10;) {
    i += 2
    console.log(i);
}
console.log("end of for2");
for (let i = 0; i < 10;) {
    i +=2
    console.log(i);
}
console.log("end of for3");
for (let i = 0; i <= 10; i +=1) {
    if(i % 2 ==0){
        console.log(i); //2458 10  홀수 주고싶으면 나머지 ==1 로 하면됨
    }
}
console.log("end of for4");

let sum = 0;
for(let i = 1; i <=10; i++) {
    console.log(sum);
    sum += i;
    console.log(sum);
}
console.log("end of for4");

for (let i = 1; i<= 10; i++) {
    if (i % 2 == 0) {
        sum += i;
    }
}
console.log(sum); //홀수 합 25 짝수합 30
document.write("<table border=1>")
// for(let k = 2; k <=9; k++) {
    let dan = 4;
      for (let num = 1; num <= 9; num++) {
        document.write("<tr>");
        console.log(dan +" * "+num+"="+(dan * num));
        document.write("<td>"+dan+"</td><td> * </td><td>" +num+ "</td><td> = </td><td>"+(dan * num)+"</td>");
        document.write("</tr>");
    }
    document.write("</table>");
// }

console.log("end of for4");