// variable1.js 
// 객체(object)=복잡한 형태의 데이터의 저장.
let person1 = {
    name: "이재현",
    age: 28,
    height: 170,
    marriage: false
}
console.log(person1.name); //getter
person1.name = "lee jae hyeon";//setter
console.log(person1.name); //영어로 출력

console.log(person1["age"]);//getter
person1["age"] = 22;//setter
console.log(person1["age"]); //22로 출력

let field = "height";
console.log(person1[field]);
field = "marriage";  //위에서 선언한게 있어서 다음부터는 let 안 써도 됨.
console.log(person1[field]);

//여러건의 정보 저장. 배열(Array)[]
let scoreArrey = [90/*0번째 배열*/, 80/*1번째 배열*/, 85/*2번째 배열*/, 70, 88];
console.log(scoreArrey[0]); //getter
scoreArrey[0] = 95; //setter
console.log(scoreArrey[0]); //getter

console.log(scoreArrey[2]);

//배열+객체.
let 자바스크립트반 = [100];
console.log(자바스크립트반[0]);

//학생의 이름과 학생번호 점수 세개의 정보를 담은 객체
let stud1 = {
    studName: "이재현",
    studNo:"1-001",
    score: 95

}
let stud2 = {
    studName: "이현재",
    studNo:"1-002",
    score: 96

}
let stud3 = {
    studName: "재현이",
    studNo:"1-003",
    score: 97

}
let stud4 = {
    studName: "현재이",
    studNo:"1-004",
    score: 98

}
let ourClass = [stud1, stud2, stud3]; //객체stud와 배열합침.
console.log(ourClass[0].studName);
console.log(ourClass[1].studNo == stud2.studNo);
console.log(ourClass[2]["score"]);
ourClass[3] = stud4; // 3위치에 stud4 추가한다.
console.log(ourClass.length); //총4개의 정보
ourClass[4] = {
    studName: "김동견",
    studNo:"1-005",
    score: 99
}
console.log(ourClass.length);//총5개의 정보 담김
//ourClass 점수의 평균.

let average = (ourClass[0]["score"]+ourClass[1]["score"]+ourClass[2]["score"]+ourClass[3]["score"]+ourClass[4]["score"])/ourClass.length;
console.log("우리반의 평균은 "+ average+"점 입니다.");
