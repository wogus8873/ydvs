 // 변수 선언.
 console.log("Hello,world"); //콘솔탭에 출력.
 let name; //변수선언.
 name = "이재현";//변수에 값 할당. => string 타입 문자열
 console.log(name);
 let age = 20; // number 타입
 console.log("이름은 " + name + " 이고, 예전 나이는 "+age+"입니다");
 
 let marriage = true; //boolean 타입 
 console.log("결혼 여부는"+true);
 marriage = false;

 console.log(typeof name); 
 name = true;
 console.log(typeof name); 

 //상수선언.
 const gender = "Men";
 // gender = "Women";

 // 변수: name1 <= 이름  height = 키, weight <=몸무게
 
 let name1 = "이재현";
 let height = 170;
 let weight = 56;
 console.log("이름은 "+name1+ " 키는 "+height+ "몸무게는 "+ weight+ "입니다.");

 // 조건식
 let averageHeight = 165;
 if(height> averageHeight){
     console.log(name1+"의 키가 "+averageHeight+"보다 큽니다.");
 }

 // 연산(+ - * / 더하기 빼기 곱하기 나누기)
 let weight1 = 66.3;
 let weight2 = 60.4;
 let weight3 = 64.2;
 let averageWeight =(weight1+weight2+weight3)/3 ;
 console.log(averageWeight);
 // 이재현의 몸무게는 평균 63.6보다 적습니다.

 console.log((weight1+weight2+weight3)/3);
