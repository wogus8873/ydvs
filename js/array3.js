//array3
const beasts = ["ant","bison","camel","duck","bison"];

// //indexOf(값)
// let result = beasts.lastIndexOf("bison");
// console.log(`bison의 위치 인덱스 값 ${result}`)
// result = beasts.indexOf("bison");
// console.log(`bison의 위치 인덱스 값 ${result}`)


// //boolean 타입 
// if(beasts.includes("bison")){
//     console.log(`bison이 존재.`)
// }else{
//     console.log(`bison은 없음.`)
// }


// function includes(name){
//     let isChecked = false;
//     for(let beast of beasts){
//         if(beast == name){
//             return true;
//         }
//         isChecked = true;
//     }
//     if(isChecked){
//         return false;    //이거 없으면 앞에서 false나고끝나서 뒤에건 검사안함
//     }
// }

// result = includes(`dog`)
// console.log(result)

//join(구분 "-") 배열 > 문자로
//split() 문자 > 배열

result = beasts.join("-").split()
console.log(result)

//slice(시작인덱스 ,종료인덱스)

result= beasts.slice(0,3)
console.log(result)

console.clear()
console.log(beasts)
//splice(위치인덱스,대체할 요소크기,대체값) : 추가 :push,unshift, 제거 : pop , shift
beasts.splice(0,-1,`bis`)
console.log(beasts)
