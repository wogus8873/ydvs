

console.log("dom실행")

let show = document.querySelector("#show");//div id = show 
let pTag = document.createElement("p") // <p>"Hello</p>
pTag.innerText = `Hello`;

show.appendChild(pTag); // show  > pTag

let btn = document.createElement("button"); //<button><//button>  
let txt = document.createTextNode("클릭") // 글자(값을) 자식요소로 변경
// btn.innerText = "클릭";  //13보다 14줄걸로 
btn.appendChild(txt)
btn.onclick = function(){   //onclik 에 기능을 달아줌 클릭하면 클릭됐습니다 출력하는 기능
    console.log("클릭됐습니다.");
}
btn.setAttribute("id","btn"); //속성지정
show.appendChild(btn);

let fruits = ["apple","banana","cherry"];
let ul = document.createElement("ul");
for(let fruit of fruits){
    let li = document.createElement("li");
    li.innerText = fruit;
    ul.appendChild(li);
}

show.appendChild(ul);

let sendp = document.querySelector("#show>p:nth-of-type(2)")
sendp.remove()
console.log(sendp);


//querySelector (선택자)  id속성이면 # class 속성이면 .
//createElement(요소)
//부모.appendChild(자식)
//remove() 삭제

// <ol>내가 좋아하는 과일<li>복숭아<> <li>사과<> <li>포도<></ol>
let fruits2 = ["복숭아", "사과", "포도"]
let txt2 = document.createTextNode("내가좋아하는과일")

ol = document.createElement("ol")
for(let fruit2 of fruits2){
    li = document.createElement("li")
    li.innerText = fruit2;
    ol.appendChild(li);
}
ol.appendChild(txt2)

show.appendChild(ol)
