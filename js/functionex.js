
let memberAry = [{memberId:"user1", memberName:"사용자1", memberAddr:"대구 중구"},
                 {memberId:"user2", memberName:"사용자2", memberAddr:"대구 서구"},
                 {memberId:"user3", memberName:"사용자3", memberAddr:"대구 북구"},
]

//자바스크립트 문자열 : "",'',``

function makeTr(memberobj){
    //tr 태그를 만들어서 반환
    // '<tr><td>${변수}</td></tr>' 이렇게하면 +안 써도됨.
    let tr = `<tr>`
    tr+=`<td>${memberobj.memberId}</td>`,
    tr+="<td>"+memberobj.memberName+"</td>",
    tr+="<td>"+memberobj.memberAddr+"</td>",
    tr+="</tr>";
    return tr
}
let titles = ["회원아이디", "회원이름", "회원주소"]
// function mekeHead() { 
//     let titles = `<tr>`
//     titles += `<th>${titles[0]}</th>`
//     titles += `<th>${titles[1]}</th>`
//     titles += `<th>${titles[2]}</th>`
//     titles += `<tr>`
//     return titles
// }
//이거 못함

let str ="<table border=1>";

for(let i=0; i<memberAry.length; i++){
    str += makeTr(memberAry[i]);  //멤버아이디 네임 어드레스가 와야한다고 생각하고 만들어야함 윗단이랑 순서바꾸면 이해 쉬움,
}
str+= "</table>"
document.write(str);