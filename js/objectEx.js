
const members = [
    {memId: "user1", memName: "홍길동", memPoint: 88},
    {memId: "user2", memName: "고길동", memPoint: 55},
    {memId: "user3", memName: "희동이", memPoint: 100}
];

function makeHead(){
    let thead = `<thead><tr><th>아이디</th><th>이름</th><th>포인트</th></tr></thead>`
    return thead
}
function makeBody(){
    let tbody = "<tbody>"
    //반복 tr
    for(let member of members){  //tr
        tbody+= "<tr>"
        for(let prop in member){  //td
            tbody+= "<td>"+member[prop]+"</td>"
        }
        tbody+= "</tr>"
    }
    tbody+="</table>"
    return tbody
}

//for of==>tr for in==>td 가지고 table 
//makeHead() ==> 타이틀(아이디, 이름, 점수)
//makebody() ==> 데이터()

let str = `<table>`;
console.log("1",str)
str+=makeHead(); //thead
console.log("2",str)
str+=makeBody(); //tbody
console.log("3",str)
str+=`</table>`;
console.log("4",str)
document.write(str);

console.log(document);