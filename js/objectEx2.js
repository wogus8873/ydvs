//objectEx2.js

let students = [];
const std1 = {
    stud_no : `1-101`,
    stud_name : `홍길동`,
    stud_math : 80, 
    stud_eng : 85
}
const std2 = {
    stud_no : `1-102`,
    stud_name : `고길동`,
    stud_math : 70, 
    stud_eng : 75
}
const std3 = {
    stud_no : `1-103`,
    stud_name : `희동이`,
    stud_math : 90, 
    stud_eng : 70
}

students[0] = std1;
students.push(std2);//Array.push(값) : 배열의 마지막 위치에 추가.
students.push(std3);

console.log(students);

function getStudents(name){
    //이름을 매개값으로 넣어주면 배열(students)의 요소에서 학생의 이름이 매개값이랑 같은 학생을 반환(return). 
    //students반복. name이 같으면 반환
    for(let std of students) {
        if(name == std.stud_name) {
            return std; //함수에서 return 끝.
            console.log("end of");
        }
    }
}

let result = getStudents("희동이")
console.log(result);