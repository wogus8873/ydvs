

let str = "Hello, world"; // string ==> string.
console.log(str.length);

console.log(str.substring(0,5));//0~5까지의 문자열을 반환
console.log(str.substring(5)); //5부터 끝까지 반환

// indexOf(값) => 문자의 index값을 반환. 
console.log(str.indexOf(","))    // 5
console.log(str.indexOf("word"))  // -1 word 가 없어서 -1 출력
console.log("Hello, world. Nice world".lastIndexOf("world")) //뒤에서 먼저 찾기

//split(구분자) => 배열로 변환.
let temp = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed dignissimos nisi corporis doloribus ad nesciunt possimus saepe incidunt, ducimus illo eos placeat mollitia ab necessitatibus, blanditiis neque nemo deserunt culpa!"
let strAry = temp.split(" "); //" "공백줘서 공백마다 객체 만드는것?
for(let word of strAry){
    if(word.indexOf("m") != -1){   //
        console.log(word);
    }
}
//console.log(strAry)

//charAt(인덱스)
console.log(str.charAt(5))  // indexOf <=> charAt

//찾을 이름 입력, 나이를 반환

let persons =[{name: "홍길동", age:20},
              {name: "고길동", age:23},
              {name: "희동이", age:12},
              {name: "마이콜", age:26}
]

function findPerson(name) {
    for(let person of persons){
        if(person.name.indexOf(name) != -1){
            return person.age;
        }
    }
    //console.log("찾는 이름이 없습니다")
    return "찾는 이름이 없습니다";
}

let findName = prompt("이름을 입력하세요: ")
let result = findPerson(findName);
console.log(result)