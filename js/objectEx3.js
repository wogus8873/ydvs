
let students = [];
const std1 = {
    stud_no : `1-101`,
    stud_name : `홍길동`,
    stud_math : 80, 
    stud_eng : 85
}
const std2 = {
    stud_no : `1-102`,
    stud_name : `고길동`,
    stud_math : 70, 
    stud_eng : 75
}
const std3 = {
    stud_no : `1-103`,
    stud_name : `희동이`,
    stud_math : 90, 
    stud_eng : 90
}
students.push(std1);
students.push(std2);//Array.push(값) : 배열의 마지막 위치에 추가.
students.push(std3);


function getStudents(mathScore) {
    //새로운 배열 선언
    let overMeth = [];
    for(let std of students){
        if(mathScore <= std.stud_math){
        overMeth.push(std);
    }
    
    }
    return overMeth;
}



let result = getStudents(80);
console.log(result);

//영어 점수 제일 큰 사람

function getMaxStudents(){
    let temp = 0;
    let student ={};
    
    for(let std of students){
        if(temp < std.stud_eng) {
           temp = std.stud_eng;
           student = std;            
        }
    }
    return student;
}
result = getMaxStudents();
console.log(result)
