//whileloop.js
// let i = 0;
// while(i<10) {
//     console.log(i);
//     i++
// }


// let isOK = true;
// while (isOK) {
//     let randomVal = parseInt(Math.random() * 10);
//     console.log(randomVal);
//     if(randomVal % 2 == 0 ) {
//         break; //반복문 종료.
//     }
// }

// let sum = 0;
// while(isOK) {
//     sum += parseInt(Math.random() * 10);
//     console.log(sum);
//     if (sum >= 100) {  //누적값이 100넘어가면 반복종료
//         break;
//     }
// }
// console.clear();

// while(true) {
//     let msg = prompt("문자입력하세요:");
//     console.log(msg);
//     if(msg == "stop") {
//         break;
//     }
// }

// Math.random => 0에서 100 까지 임의의 수를 생성  = >  randomVal ;
//prompt("숫자를 입력하세요 : ") =>inputVal
//입력한값이 임의의값보다 큽니다.
let num2=parseInt(Math.random()*100);
console.log(num2)

while (true) { 
    let msg = prompt("숫자를 입력하세요: ");
    console.log(msg);
    if(msg > num2) {
        console.log("입력한 값이 임의의 값보다 큽니다.");
    }else if(msg < num2){
        console.log("입력한 값이 임의의 값보다 작습니다.");
    }else {
        break;
    }
}


console.log("and while");