//condition.js
let score = parseInt(Math.random() * 60)+40;  //40~100임의의 정수값
let pass = "";

console.log(score);
if(score >= 60) {
    pass="합격";
 } else { //else 이외의 경우.
     pass="불합격";
 }
 console.log(pass);


pass = (score >= 60) ? "합격" : "불합격"; //간소화한거
console.log(pass);


//이프
if(score >= 90) {
    pass = "A";
} else { //90 아래
    if(score >= 80) {
        pass = "B";
    } else { // 80 아래
        if(score >= 70) {
            pass = "C";
        } else{ //70 아래
            pass = "D";
        }
    }
}
console.log(pass);

//위에꺼 간소화 (if 조건문이 하나고 4개의 조건으로 한번만 실행)
//95 85 75 = A+ B+ C+
if (score >= 95) {
    pass = "A+"
}else if(score >= 90) {
    pass = "A"
}else if(score >= 80) { //이런식으로도 가능.
    if(score >= 85){
        pass = "B+"
    }else{
        pass = "B"
    }
}else if (score >= 80) {
    pass = "B"
}else if (score >= 75) {
    pass = "C+"
}else if (score >= 70) {
    pass = "C"
}else{
    pass = "D"
}
console.log(pass);
