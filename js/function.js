// {name :"" , age:"", showInfo: "기능"}

function sayHello(msg, name) { //매개변수.
    console.log(msg+","+name+"!!");
}

sayHello("안녕하세요","홍길동"); // 매개값 대입 함수호출(call)
sayHello("반가워요","고길동"); // 매개값 대입 함수호출(call)

function sum(num1, num2) {
    let result = num1+num2;
    return result;// 함수를 호출한 영역으로 결과값을 반환(return)
}

let result = sum(10, 20); // 첫번째값과 두번째를 더하고 result에 담고 그걸 반환한다 e다시 sum을 result에 담을 수 있는게 return
console.log("결과:"+sum(result,30));


let member = {
    memberId : "user1",
    memberName : "사용자1",
    memberAddr : "대구 중구"
}
function showInfo(obj) {
    console.log("회원아이디: "+obj.memberId+" 회원 이름: "+obj.memberName+" 회원주소: "+obj.memberAddr);
}

showInfo(member); //멤버를 설정함 위에 let으로 설정한것
showInfo({
    memberId : "user1",
    memberName : "사용자1",
    memberAddr : "대구 중구"
});

function makeList(obj){
    //<ul><li>..</li></ul>
    let li = "<li>";
    li += "id"+ obj.memberId;
    li += "name"+ obj.memberName;
    li += "Addr"+ obj.memberAddr;
    li += "</li>";
    return li;
}

let memberAry = [{memberId:"user1", memberName:"사용자1", memberAddr:"대구 중구"},
                 {memberId:"user2", memberName:"사용자2", memberAddr:"대구 서구"},
                 {memberId:"user3", memberName:"사용자3", memberAddr:"대구 북구"},
]

//document.write(str);
let str = "<ul>";
for (let i=0; i<memberAry.length; i++) {
   str += makeList(memberAry[i]);
}
str += "</ul>"
document.write(str);