//object => {name:홍길동 , age:20, showInfo: function() {...}}
//객체: 속성(필드:값), 메소드(기능)


const member = {
    memberId: "user1",
    memberName: "고길동",
    memberInfo: function(){  // 메소드
        return `아이디:${this.memberId}이고 이름은 ${this.memberName}`;
    }
}

console.log(member.memberId);              //방법1
console.log(member["memberName"]);         //방법2
console.log(member.memberInfo());   //함수호출 메소드호출.

//객체의 속성을 반복하며 반환해주는 for in
//member.memberId / member["memberId"]
for(let prop in member){
    console.log(prop,member[prop]); 
    if(prop == `memberInfo`){
        console.log(member[prop]())
    }
}

// for .. of : 배열 . for .. in : 객체.
const members = [
    {memId: "user1", memName: "홍길동", memPoint: 88},
    {memId: "user2", memName: "고길동", memPoint: 55},
    {memId: "user3", memName: "희동이", memPoint: 100}
];
console.log(members);
//확장 for 쓴다 (for of)
for(let member of members){ //멤버즈의 갯수만ㅇ큼 반복. 멤버에 담아서 값을 처리할게용. 데이터건수반복
    if(member.memPoint > 80){
    for(let prop in member){//속성 갯수 반복.
        if(prop == `memName`){
        
        console.log(`속성:${prop}, 값:${member[prop]}`);
    }
    } 
    console.log(`===============================`);
}   
}