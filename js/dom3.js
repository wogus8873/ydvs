//don3.js
let members = [{name: "홍길동",age:23,birth:"2001-03-02" },
               {name: "고길동",age:22,birth:"2002-03-02" },
               {name: "희동이",age:21,birth:"2003-03-02" }
];

window.onload = loadCallback;
function loadCallback() {
    //최초 리스트 출력.
    let table = document.getElementById("list");
    table.setAttribute("border","1")
    table.append(makeThead());       // <thead>
    table.append(makeTbody());       // <tbody>
    //추가 기능
    document.getElementById("btn").addEventListener("click",addFunc)  //이벤트 타입, 함수
    document.getElementById("delBtn").addEventListener("click", deleteFnc)
    // document.getElementById("changeBth").addEventListener("click", modifyFnc)
    //mouseover, mouseout 이벤트
    let trs = document.querySelectorAll("tbody>tr")
    for(let tr of trs){
        tr.addEventListener("mouseover",function(){tr.style.backgroundColor = "yellow"})
        tr.addEventListener("mouseout",function(){tr.style.backgroundColor = ""})
    }
    console.log(trs)
}

// function modifyFnc(){
//     let name = document.getElementById("name").value;
//     let age = document.getElementById("age").value;

//     let targetTr = document.querySelectorAll("tbody tr")
//     for(let tr of targetTr){
//         if(tr.children[0].innerText == name){
//             tr.children[1].innerText = age;
//         }
//     }
// }


function deleteFnc(){
    let allChk = document.querySelectorAll(`tbody input[type="checkbox"]:checked`);
    console.log(allChk);
    for(let chk of allChk){
        chk.parentElement.parentElement.remove();
        
    }
}


function addFunc(){
    let name = document.querySelector("#name").value;
    let age = document.querySelector("#age").value;
    let birth = document.querySelector("#birth").value;
    let props = {name: name,age: age,birth: birth}
    //반복.
    // let tr = makeRow(props);
    document.querySelector("#list>tbody").append(makeRow(props))
}

function makeThead(){
    //index에 thead만드는중
    let title = ["이름","나이","생일","삭제"];
    let thead = document.createElement("thead");
    let tr = document.createElement("tr");
    for(let head of title){
        let th = document.createElement("th");
        th.innerText = head;
        tr.append(th);
    }
    //전체선택.
    let th = document.createElement("th");
    let check = document.createElement("input");
    check.setAttribute("type", "checkbox");
    th.append(check);
    tr.append(th);
    check.addEventListener("click", allCheckFnc)

    thead.append(tr);
    return thead;
}


function allCheckFnc(){
    let allChk = document.querySelectorAll(`input[type="checkbox"]`);
    //전체체크박스를 대상으로 타이틀에 있는 체크박스와 같은 값으로 변경.
    for(let chk of allChk){
        chk.checked = this.checked;
    }
}


function makeTbody(){
    // tbody tr td 값  /td td 값 ... /tbody
    let tbody = document.createElement("tbody");
    for(let member of members){ //
        let tr = makeRow(member)
        //반복. 
        tbody.append(tr);
    }
    return tbody
}


function makeRow(member={}){   //이름 나이 생일 받으면 tr 생성
    //반복
    let tr = document.createElement("tr");
    //mouseover, mouseout 이벤트
    tr.addEventListener("mouseover", mouseOverFnc);
    tr.addEventListener("mouseout",function(){tr.style.backgroundColor = ""})
    tr.addEventListener("click",clickFnc,false)
        for(let prop in member){  // 객체 in  
            //반복
            let td = document.createElement("td");
            let txt = document.createTextNode(member[prop]);
            td.append(txt);
            tr.append(td);
        }
        //td 값 /td => td button 삭제  /button /td
        let td = document.createElement("td");
        let btn = document.createElement("button")
        btn.addEventListener("click",function(){
            console.log("click button")
        event.stopPropagation()
        tr.remove
        },false) //btn. .  말고 tr. 써도됨 부모불러와 부모불러와!!임.
        btn.innerText = "삭제"
        
        td.append(btn)
        tr.append(td)
        console.log(tr)
        

        td = document.createElement("td");
        let check = document.createElement("input");
        check.setAttribute("type", "checkbox");
        td.append(check);
        tr.append(td);
        return tr
}



function mouseOverFnc(){
    let tr = this;  //event대상 객체
    console.log(tr.style.backgroundColor = "yellow");//{}
}

function clickFnc(){
    console.log("tr click")
    let tr = this;
    console.log(tr.style.backgroundColor = "yellow");//{}
    document.getElementById("name").value = tr.children[0].innerText;
    document.getElementById("age").value = tr.children[1].innerText;
    document.getElementById("birth").value = tr.children[2].innerText;
    event.stopPropagation();
}