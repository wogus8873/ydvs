

//책 : Book (제목 : title , 저자 : author, 출판사 : press , 가격 : price

class Book {
    constructor(title, author, press, price){
        this.title = title;
        this.author = author;
        this.press = press;
        this.price = price;
    }
}


//다산북스, 김영사 , 범우사 , 두건씩
let b1 = new Book(`모르겠음`, `이재현`, `다산북스`, `1억`);
let b2 = new Book(`전혀 모르겠음`, `재현이`, `다산북스`, `2억`);
let b3 = new Book(`진짜 모르겠음`, `이째현`, "김영사", `3억`);
let b4 = new Book(`가짜 모르겠음`, `째현이`, "김영사", `4억`);
let b5 = new Book(`정말 모르겠음`, `고길동`, `범우사`, `5억`);
let b6 = new Book(`리얼 모르겠음`, `희동이`, `범우사`, `6억`);

let books = []; //6건 
books.push(b1);
books.push(b2);
books.push(b3);
books.push(b4);
books.push(b5);
books.push(b6);

function findBooks(press){  //출판사 이름 입력하면 
   
     for(let book of books){
        if(book.press == press){
            return book
        }
     }
}  

let result = findBooks("김영사");
console.log(result);